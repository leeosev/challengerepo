/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author smeds
 */
public class Coordinate {

    Building building;
    Antenna antenna;
    
    public Coordinate() {
        building = null;
        antenna = null;
    }


    public Building getBuilding() {
        return building;
    }

    public void setBuilding(Building building) {
        this.building = building;
    }

    public Antenna getAntenna() {
        return antenna;
    }

    public void setAntenna(Antenna antenna) {
        this.antenna = antenna;
    }
    public boolean hasAntenna(){
        return antenna != null;
    }
    public boolean hasBuilding(){
        return building != null;
    }

    @Override
    public String toString() {
        return "Coordinate{" + "building=" + building + ", antenna=" + antenna + '}';
    }
    
}

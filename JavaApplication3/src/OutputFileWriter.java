/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
/**
 *
 * @author giacomogori
 */
public class OutputFileWriter {
   
   
private File outputFile;

    public void writeOutput(Antenna[] antennas) throws IOException{
        outputFile = new File("output6.txt");
        FileWriter fileout = new FileWriter(outputFile);
        BufferedWriter filebuf = new BufferedWriter(fileout);
        PrintWriter printout = new PrintWriter(filebuf);
        //scrivo il numero di antenne
        printout.println(antennas.length);
        //scrivo le coordinate di ciascuna antenna
        for(int i=0; i<antennas.length; i++){
            printout.println(antennas[i].getId() + " " + antennas[i].getX() + " " + antennas[i].getY());
        }
        printout.close();

    }
    
}

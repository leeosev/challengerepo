
import java.util.HashSet;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author smeds
 */
public class Antenna {
    private int id;
    private int x, y;
    private int range;
    private int connectionSpeed;
    public HashSet<Building> buildings;

    public Antenna(int id, int x, int y, int range, int connectionSpeed) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.range = range;
        this.connectionSpeed = connectionSpeed;
        buildings = new HashSet<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }    
    
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public int getConnectionSpeed() {
        return connectionSpeed;
    }

    public void setConnectionSpeed(int connectionSpeed) {
        this.connectionSpeed = connectionSpeed;
    }

    @Override
    public String toString() {
        return "Antenna{" + "id=" + id + ", x=" + x + ", y=" + y + ", range=" + range + ", connectionSpeed=" + connectionSpeed + '}';
    }

    

    
    
    
}

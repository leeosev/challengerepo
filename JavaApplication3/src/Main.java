/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author smeds
 */
import java.io.IOException;
import java.lang.Math;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    public static void main(String[] args) {
        String testFile = "data_scenarios_f_tokyo.in";
        InputFileReader ifr = new InputFileReader(testFile);
        
        ifr.readInput();
        Coordinate[][] matrix = ifr.createMatrix();
        print2D(matrix);
       
        processMatrix(matrix,ifr.antennas);
        int sc = calculateScore(ifr.buildings,ifr.reward);
        System.out.println("SCORE: "+sc);
        OutputFileWriter ofw = new OutputFileWriter();
        
        try {
            ofw.writeOutput(ifr.antennas);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    public static int calculateScore(Building[] buildings, int rew){
        int sum = 0;
        boolean reward = true;
        for(int i = 0; i < buildings.length; i++) {
            Building b = buildings[i];
            if(b.antenna == null){
                reward = false;
            }
            else {
                int sc = b.score;
                sum+=sc;
            }
            
        }
        if(reward){
            System.out.println("REWARD");
            sum+=rew;
        }
        return sum;
    }
    
    
    public static int dist(int x1, int y1, int x2, int y2){
        return Math.abs(x1-x2) + Math.abs(y1-y2);
    }
    public static int score(Building b, Antenna a,int d)
    {
        return b.getConnectionSpeed()*a.getConnectionSpeed() - b.getLatency()*d;
    }
    public static void processMatrix(Coordinate[][] matrix, Antenna[] antennas){
        for(Antenna a : antennas){
            for(int i = a.getX()-a.getRange(); i<a.getX()+a.getRange();i++)
                for(int j = a.getY()-a.getRange(); j<a.getY()+a.getRange();j++){
                    int d = dist(i,j,a.getX(),a.getY());
                    if(i>=0 && i<matrix.length && j>=0 && j<matrix[0].length && d < a.getRange()){
                        if(matrix[i][j].hasBuilding()){
                            matrix[i][j].getBuilding().setAntenna(a,score(matrix[i][j].getBuilding(), a, d));
                        }
                    }
                }
                    
        }
    }
    public static void print2D(Coordinate mat[][]) {
// Loop through all rows 
        for (int i = 0; i < mat.length; i++) {
// Loop through all elements of current row 
            for (int j = 0; j < mat[i].length; j++) {
                //System.out.print(mat[i][j].toString() + " ");
            }
            //System.out.println('\n');
        }
    }
}

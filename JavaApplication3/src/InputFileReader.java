
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.Math;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author smeds
 */
public class InputFileReader {

    private String inputName;
    private BufferedReader br;
    private File inputFile;

    public int w, h, numBuild, numAnt, reward;
    public Building[] buildings;
    public Antenna[] antennas;

    public InputFileReader(String in) {
        inputName = "src/" + in;
        inputFile = new File(inputName);
        try {
            br = new BufferedReader(new FileReader(inputFile));
        } catch (FileNotFoundException ex) {
            System.out.println("Not found: " + inputFile.getAbsolutePath());
        }
    }

    public void readInput() {
        String st;
        String arr[];
        try {
            // First line of the file: Width and Height of the grid
            st = br.readLine();
            arr = st.split(" ");
            w = Integer.parseInt(arr[0]);
            h = Integer.parseInt(arr[1]);

            // Second line: no. of antennas, no. of buildings, reward
            st = br.readLine();
            arr = st.split(" ");
            numBuild = Integer.parseInt(arr[0]);
            numAnt = Integer.parseInt(arr[1]);
            reward = Integer.parseInt(arr[2]);

            this.buildings = new Building[numBuild];
            this.antennas = new Antenna[numAnt];

            // Next numBuild lines: x-coord, y-coord of building, latency weight, connection weigth
            for (int i = 0; i < numBuild; i++) {
                int x, y, connectionSpeed, latency;
                st = br.readLine();
                arr = st.split(" ");

                x = Integer.parseInt(arr[0]);
                y = Integer.parseInt(arr[1]);
                latency = Integer.parseInt(arr[2]);
                connectionSpeed = Integer.parseInt(arr[3]);

                buildings[i] = new Building(i, x, y, connectionSpeed, latency);
                //System.out.println(buildings[i]);
            }

            // Next numAnt lines: range, connection speed of antenna
            for (int i = 0; i < numAnt; i++) {
                int range;
                int connectionSpeed;

                st = br.readLine();
                arr = st.split(" ");

                range = Integer.parseInt(arr[0]);
                connectionSpeed = Integer.parseInt(arr[1]);

                antennas[i] = new Antenna(i, 0, 0, range, connectionSpeed);
                //System.out.println(antennas[i]);
            }
        } catch (IOException ex) {
            System.out.println("Not Found");
        }
    }

    public Coordinate[][] createMatrix() {
        Coordinate[][] res = new Coordinate[w][h];
        int countAntenna = 0;
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                if (res[i][j] == null) {
                    res[i][j] = new Coordinate();
                }
            }
        }
        //Add antenna in the same position of every buildings
        for (Building b : buildings) {
            res[b.getX()][b.getY()].setBuilding(b);
            if(countAntenna < antennas.length) {
                res[b.getX()][b.getY()].setAntenna(antennas[countAntenna]);
                antennas[countAntenna].setX(b.getX());
                antennas[countAntenna].setY(b.getY());
                countAntenna++;
            }
        }
       
        //This initialize randomly the remaining antennas
        
        int i = countAntenna;
        while (i < antennas.length) {
            int x = (int) (Math.random() * w);
            int y = (int) (Math.random() * h);
            if(!res[x][y].hasAntenna())
            {
                res[x][y].setAntenna(antennas[i]);
                antennas[i].setX(x);
                antennas[i].setY(y);
                i++;
                
            }
        }
        return res;
    }

}

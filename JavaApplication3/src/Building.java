
import com.sun.tools.javac.util.Pair;
import java.util.PriorityQueue;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author smeds
 */
public class Building {
    private int id;
    private int x;
    private int y;
    private int connectionSpeed;
    private int latency;
    //public PriorityQueue<Pair<Antenna,Integer>> antennas;
    public Antenna antenna;
    public int score;
    
    public Building(int id, int x, int y, int connectionSpeed, int latency) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.connectionSpeed = connectionSpeed;
        this.latency = latency;
        this.score=0;
        //antennas = new PriorityQueue<>((x1,y1)->Integer.compare(x1.snd,y1.snd));
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getConnectionSpeed() {
        return connectionSpeed;
    }

    public void setConnectionSpeed(int connectionSpeed) {
        this.connectionSpeed = connectionSpeed;
    }

    public int getLatency() {
        return latency;
    }

    public void setLatency(int latency) {
        this.latency = latency;
    }
    public Pair<Antenna,Integer> setAntenna(Antenna a, int d){
        Pair<Antenna,Integer> res;
        if(d>score){
            res= new Pair<>(this.antenna,this.score);
            if(antenna != null)
            {
                antenna.buildings.remove(this);
            }
            this.antenna = a;
            a.buildings.add(this);
            this.score = d;
            return res;
        }
        else return null;
    }
    @Override
    public String toString() {
        return "Building{" + "id=" + id + ", x=" + x + ", y=" + y + ", connectionSpeed=" + connectionSpeed + ", latency=" + latency + '}';
    }

    
}

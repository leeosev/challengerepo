/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author leo
 */
import java.lang.Math;
import java.util.HashSet;
import java.util.Set;

public class optimizer {

    public Antenna[] antennas;
    public Coordinate[][] matrix;

    public optimizer(Antenna[] antennas, Coordinate[][] matrix) {
        this.antennas = antennas;
        this.matrix = matrix;
    }

    public int dist(int x1, int y1, int x2, int y2) {
        return Math.abs(x1 - x2) + Math.abs(y1 - y2);
    }

    public int score(Building b, Antenna a, int d) {
        return b.getConnectionSpeed() * a.getConnectionSpeed() - b.getLatency() * d;
    }

    public void optimize(int maxIteration) {
        int t = 0;
        boolean modified = false;
        while (t < maxIteration && modified) {
            for (Antenna a : antennas) {
                int xmin = Math.max(0, a.getX() - 1);
                int xmax = Math.min(matrix.length, a.getX() + 1);
                int ymin = Math.max(0, a.getY() - 1);
                int ymax = Math.min(matrix[0].length, a.getY() + 1);
                int posRnd = (int) Math.floor(Math.random() * matrix.length * matrix[0].length);
                int xRnd = posRnd / matrix.length;
                int yRnd = posRnd % matrix.length;
                int xminRnd = Math.max(0, xRnd - 1);
                int xmaxRnd = Math.min(matrix.length, xRnd + 1);
                int yminRnd = Math.max(0, yRnd - 1);
                int ymaxRnd = Math.min(matrix[0].length, yRnd + 1);
                int x = 0, y = 0;
                long s = 0;
                for (int i = xmin; i < xmax; i++) {
                    for (int j = ymin; j < ymax; j++) {
                        for (int i1 = i - a.getRange(); i1 < i + a.getRange(); i++) {
                            for (int j1 = j - a.getRange(); j1 < j + a.getRange(); j++) {
                                int d = dist(i1, j1, i, j);
                                if (i >= 0 && i < matrix.length && j >= 0 && j < matrix[0].length && d < a.getRange()) {
                                    if (matrix[i1][j1].hasBuilding()) {
                                        int ls = score(matrix[i1][j1].getBuilding(), a, d);
                                        if(ls > s){
                                            s = ls;
                                            x = i;
                                            y = j;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                for (int i = xminRnd; i < xmaxRnd; i++) {
                    for (int j = yminRnd; j < ymaxRnd; j++) {
                        for (int i1 = i - a.getRange(); i1 < i + a.getRange(); i++) {
                            for (int j1 = j - a.getRange(); j1 < j + a.getRange(); j++) {
                                int d = dist(i1, j1, i, j);
                                if (i >= 0 && i < matrix.length && j >= 0 && j < matrix[0].length && d < a.getRange()) {
                                    if (matrix[i1][j1].hasBuilding()) {
                                        int ls = score(matrix[i1][j1].getBuilding(), a, d);
                                        if(ls > s){
                                            s = ls;
                                            x = i;
                                            y = j;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (x!=a.getX() && y!=a.getY()){
                    modified = true;
                    matrix[a.getX()][a.getY()].setAntenna(null);
                    matrix[x][y].setAntenna(a);
                    a.setX(x);
                    a.setY(y);
                }
            }
        }
    }
}
